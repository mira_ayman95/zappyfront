import { Component, HostListener } from '@angular/core';
import { TwitterService } from './services/twitter.service';
import { TweetModel } from './models/tweet-model';
import { AlertsService } from 'angular-alert-module';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'zappy';
  private tweets = [];
  private listOfTweets: Array<TweetModel> = [];
  private page = 0;

  ngOnInit() {
    this.page = 0;
  }

  constructor(private twitterService: TwitterService, private alerts: AlertsService) {
    this.page = 0;
    this.twitterService.getTweets(this.page).subscribe((response) => {
      if (response['tweets']) {
        response['tweets'].forEach(tweet => {
          tweet['username'] = tweet['user']['username']
          tweet['fullName'] = tweet['user']['name']
          tweet['profileImg'] = tweet['user']['profileImg']
        })
        this.listOfTweets = response['tweets'];
        this.page++;
      };
    }, (response) => {
      if (response['status'] == 400) {
        this.alerts.setDefaults('timeout', 5)
        this.alerts.setMessage(response['error']['error'], 'error');
      }
    });
  }

  @HostListener('window:scroll', [])
  onScroll(): void {
    if ((window.innerHeight + window.scrollY) >= document.body.offsetHeight) {
      this.twitterService.getTweets(this.page).subscribe((response) => {
        if (response['tweets']) {
          response['tweets'].forEach(tweet => {
            tweet['username'] = tweet['user']['username']
            tweet['fullName'] = tweet['user']['name']
            tweet['profileImg'] = tweet['user']['profileImg']
            this.listOfTweets.push(tweet);
          })
          this.page++;
        };
      }, (response) => {
        if (response['status'] == 400) {
          this.alerts.setDefaults('timeout', 10)
          this.alerts.setMessage(response['error']['error'], 'error');
          return;
        }
      });
    }
  }

}

