import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import { TweetModel } from '../models/tweet-model';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class TwitterService {

  baseUrl = 'http://localhost:4000';

  constructor(private httpClient: HttpClient) {
  }

  getTweets(page):Observable<any> {
    return this.httpClient.get(this.baseUrl + '/api/tweets/'+(page*10)+'/10');
  }
}
