import {Component, Input, OnInit} from '@angular/core';
import {TweetModel} from '../models/tweet-model';

@Component({
  selector: 'app-tweet',
  templateUrl: './tweet.component.html',
  styleUrls: ['./tweet.component.css']
})
export class TweetComponent implements OnInit {

  @Input() model: TweetModel;


  constructor() {
  }

  ngOnInit() {
  }

}
