export interface TweetModel {
  username: string;
  fullName: string;
  profileImg: string;
  created_at : number;
  text : string,
  retweet_count: number,
  reply_count: number,
  favorite_count: number,
  media: Array<{ media_url: string, value: string }>;
}
